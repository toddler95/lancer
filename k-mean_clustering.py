import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)


import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt2
import matplotlib.cm as cm
#matplotlib inline
from sklearn import preprocessing
from subprocess import check_output

data = pd.read_csv('lung-cancer.csv')

# Scaling the dataset
datas = pd.DataFrame(preprocessing.scale(data.iloc[1:,1:]))
datas.columns = list(data.iloc[1:,1:].columns)
datas[['c']] = data[[0]]

# Creating the high dimensional feature space X
data_drop = datas.drop(['c'],axis=1)
X = data_drop.values


#Creating a 2D visualization to visualize the clusters
from sklearn.manifold import TSNE
tsne = TSNE(verbose=1, perplexity=3, method = "exact", n_iter= 250)
Y = tsne.fit_transform(X)


from sklearn.cluster import KMeans
kmns = KMeans(n_clusters=2, init='k-means++', n_init=10, max_iter=300, tol=0.0001, precompute_distances='auto', verbose=0, random_state=None, copy_x=True, n_jobs=1, algorithm='auto')
kY = kmns.fit_predict(X)

f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)

ax1.scatter(Y[:,0],Y[:,1],  c=kY, cmap = "jet", edgecolor = "None", alpha=0.35)
ax1.set_title('k-means clustering plot')

ax2.scatter(Y[:,0],Y[:,1],  c = datas['c'], cmap = "jet", edgecolor = "None", alpha=0.35)
ax2.set_title('Actual clusters')

